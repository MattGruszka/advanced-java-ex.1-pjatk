package gorgeousweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import servlets.HelloServlet;

public class TestHelloServlet extends Mockito {


	@Test
	public void checkValidation() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
					
		 when(request.getParameter("kwotaKredytu")).thenReturn("");
		when(response.getWriter()).thenReturn(writer);
		
		new HelloServlet().doPost(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void checkPdfDisplay() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
					
		 
		when(response.getWriter()).thenReturn(writer);
		
		new HelloServlet().doGet(request, response);
		
		
		verify(writer).println("<h1> PDF zostal pobrany do folderu z projektem. </h1>");
	}
}
