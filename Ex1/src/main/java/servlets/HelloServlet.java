package servlets;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import models.OpisRaty;

@WebServlet("/harmonogram")
public class HelloServlet extends HttpServlet {
	
	String kwotaKredytu = null;
	String iloscRat = null;
	String oprocentowanie = null;
	String oplataStala = null;
	String rodzajMalejaca = null;
	String rodzajStala = null;
	private static final long serialVersionUID = 1L;
	
	ArrayList<OpisRaty> list = new ArrayList<OpisRaty>();;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		 kwotaKredytu = request.getParameter("kwotaKredytu");
		 iloscRat = request.getParameter("iloscRat");
		 oprocentowanie = request.getParameter("oprocentowanie");
		 oplataStala = request.getParameter("oplataStala");
		 rodzajMalejaca = request.getParameter("rodzajMalejaca");
		 rodzajStala = request.getParameter("rodzajStala");


		if (kwotaKredytu == null || kwotaKredytu.equals("")  ||
				iloscRat == null || iloscRat.equals("")  ||
				oprocentowanie == null || oprocentowanie.equals("")  ||
				oplataStala == null || oplataStala.equals("")  ||
				kwotaKredytu == null || kwotaKredytu.equals("")  || ( 
				(rodzajMalejaca == null  && rodzajStala == null  ) ||
				(rodzajMalejaca != null  && rodzajStala != null  )))
		{
			response.sendRedirect("/");
		} else {
			if(rodzajMalejaca !=null) {
				obliczRateMalejaca();
			}
			if(rodzajStala !=null) {
				obliczRateStala();
			}
			
			
			response.setContentType("text/html");
			
			response.getWriter().println("<style>\n" + 
					"table, th, td {\n" + 
					"    border: 1px solid black;\n" + 
					"}\n" + 
					"</style><table style=\"width:100%\">\n" + 
					"  <tr>\n" + 
					"    <th>Nr raty</th>" + 
					"    <th>Kwota Kapitalu</th> " + 
					"    <th>Kwota odsetek</th>" + 
					"    <th>Oplaty stale</th>" + 
					"    <th>Calkowita kwota raty</th>" + 
					"  </tr>" + 
					displayTable(list)
					+"</table>"
					
					+
					"<form action=\"harmonogram\" method=\"GET\">"+
					"<input type=\"submit\" value=\"pobierz PDF\"/>"
	
					);	
		}

	}
	
	public void obliczRateMalejaca() {
		
		float czescKapitalowa = Integer.parseInt(kwotaKredytu)/Integer.parseInt(iloscRat);
		float kwotaZmniejszajacaSie = Integer.parseInt(kwotaKredytu);
		int numerRaty = 1;
		float rata;
		float czescOdsetkowa;
		
		for(int i=0; i<Integer.parseInt(iloscRat); i++) {
			
			czescOdsetkowa = kwotaZmniejszajacaSie * (Float.parseFloat(oprocentowanie)/100)/12;
			rata =  czescKapitalowa + czescOdsetkowa + Integer.parseInt(oplataStala);
			
			OpisRaty or = new OpisRaty(numerRaty,rata,kwotaZmniejszajacaSie,czescOdsetkowa, oplataStala);
			list.add(or);
			kwotaZmniejszajacaSie = kwotaZmniejszajacaSie - rata;
			numerRaty++;
			
		}
	}
	
	public void obliczRateStala() {
		new ArrayList<OpisRaty>();		
		float kwotaZmniejszajacaSie = Integer.parseInt(kwotaKredytu);
		int numerRaty = 1;
		float rata;
		float czescOdsetkowa;
		
		float q = 1 + ((Float.parseFloat(oprocentowanie)/100)/12);
		float temp = (float) Math.pow(q, Integer.parseInt(iloscRat));
		czescOdsetkowa =  Float.parseFloat(kwotaKredytu) * temp * ((q	-1)/(temp-1)) - (kwotaZmniejszajacaSie/Integer.parseInt(iloscRat));
		rata = czescOdsetkowa + Float.parseFloat(oplataStala);	
		
		for(int i=0; i<Integer.parseInt(iloscRat); i++) {
						
			OpisRaty or = new OpisRaty(numerRaty,rata,kwotaZmniejszajacaSie,czescOdsetkowa, oplataStala);
			list.add(or);
			numerRaty++;
			
		}	
	}
	
	public String displayTable(ArrayList arl) {
		String resultStr = "";
		for(int i=0; i<arl.size(); i++) {
			OpisRaty op = (OpisRaty) arl.get(i);
			resultStr = resultStr + "<tr>"+
		 "<td>" + op.nrRaty + "</td>" + 
		 "<td>" + op.kwotaZmniejszajacaSie + "</td>" +
		 "<td>" + op.czescOdsetkowa + "</td>" +
		 "<td>" + op.oplataStala + "</td>" +
		 "<td>" + op.rata + "</td>" +
					"</tr>";
		}
		return resultStr;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		response.setContentType("text/html");
		response.getWriter().println("<h1> PDF zostal pobrany do folderu z projektem. </h1>");
		
		try {
			createPDF();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
		
		public void createPDF() throws FileNotFoundException, DocumentException {
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream("HarmonogramRat.pdf"));
			 
			document.open();
			 
			PdfPTable table = new PdfPTable(5);
			addTableHeader(table);
			addRows(table);
			 
			document.add(table);
			document.close();
			
			
		}
		
		private void addTableHeader(PdfPTable table) {
			
			String[] columnTitles = {"Nr raty" ,"Kwota Kapitalu" , "Kwota odsetek", "Oplaty stale" ,
			                        "Calkowita kwota raty"};
			
			for(int i=0; i<5; i++) {
		        PdfPCell header = new PdfPCell();
		        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
		        header.setBorderWidth(2);
		        header.setPhrase(new Phrase(columnTitles[i]));
		        table.addCell(header);
			}

		}
		
		private void addRows(PdfPTable table) {
			
			for(int i=0; i<list.size(); i++) {
				OpisRaty op = (OpisRaty) list.get(i);
				
				   table.addCell(op.nrRaty + "");
				    table.addCell(op.kwotaZmniejszajacaSie+ "");
				    table.addCell(op.czescOdsetkowa+"");
				    table.addCell(op.oplataStala+"");
				    table.addCell(op.rata+"");
		   
			}
		
		}
}