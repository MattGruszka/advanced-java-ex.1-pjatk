package models;

public class OpisRaty {

	public int nrRaty;
	public float rata;
	public float kwotaZmniejszajacaSie;
	public float czescOdsetkowa;
	public String oplataStala;
	
	public OpisRaty(int nrRaty, float rata, float kwotaZmniejszajacaSie, float czescOdsetkowa, String oplataStala) {
		this.nrRaty = nrRaty;
		this.rata = rata;
		this.kwotaZmniejszajacaSie = kwotaZmniejszajacaSie;
		this.czescOdsetkowa = czescOdsetkowa;
		this.oplataStala = oplataStala;
	}
}

